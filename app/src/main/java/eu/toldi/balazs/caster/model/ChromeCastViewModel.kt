package eu.toldi.balazs.caster.model

import android.net.wifi.WifiManager
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import su.litvak.chromecast.api.v2.ChromeCast
import su.litvak.chromecast.api.v2.ChromeCasts
import su.litvak.chromecast.api.v2.ChromeCastsListener
import java.lang.String
import java.net.InetAddress
import java.net.NetworkInterface
import java.net.UnknownHostException


class ChromeCastViewModel : ViewModel(), ChromeCastsListener {
    private val _chromecasts: MutableLiveData<List<ChromeCast>> =
        MutableLiveData<List<ChromeCast>>(listOf())
    val chromeCasts: LiveData<List<ChromeCast>>
        get() = _chromecasts

    init {

        ChromeCasts.registerListener(this)
    }

    fun addChromecast(chromeCast: ChromeCast) {
        _chromecasts.postValue(_chromecasts.value!!.plus(chromeCast))
    }

    override fun newChromeCastDiscovered(chromeCast: ChromeCast?) {
        if (chromeCast != null && _chromecasts.value!!.firstOrNull { it.address == chromeCast.address} == null) {
            Log.i(null, "Found ${chromeCast.title}")
            _chromecasts.postValue(_chromecasts.value!!.plus(chromeCast))
        }
    }

    override fun chromeCastRemoved(chromeCast: ChromeCast?) {
        if (chromeCast != null) {
            Log.i(null,"Lost ${chromeCast.title}")
            _chromecasts.postValue(_chromecasts.value!!.minus(chromeCast))
        }
    }

    fun refresh() {
       _chromecasts.value = emptyList()
        viewModelScope.launch(IO) {
            ChromeCasts.restartDiscovery(getIPv4Address())
        }
    }

    fun startScanning() {
        viewModelScope.launch(IO) {
            val address = getIPv4Address()
            if (address.toString().startsWith('/')) {
                Log.e("Caster", address.toString().drop(1))
                ChromeCasts.startDiscovery(InetAddress.getByName(address.toString().drop(1)))
            } else {
                ChromeCasts.startDiscovery(address)
            }
        }
    }
    fun getIPv4Address(): InetAddress? {
        NetworkInterface.getNetworkInterfaces().toList().forEach { interf ->
            interf.inetAddresses.toList().forEach { inetAddress ->
                if (!inetAddress.isLoopbackAddress && inetAddress.hostAddress.indexOf(':') < 0) {
                    return inetAddress
                }
            }
        }
        return null
    }

    private fun getDeviceIpAddress(wifi: WifiManager): InetAddress? {
        var result: InetAddress? = null
        try {
            // default to Android localhost
            result = InetAddress.getByName("10.0.0.2")

            // figure out our wifi address, otherwise bail
            val wifiinfo = wifi.connectionInfo
            val intaddr = wifiinfo.ipAddress
            val byteaddr = byteArrayOf(
                (intaddr and 0xff).toByte(), (intaddr shr 8 and 0xff).toByte(),
                (intaddr shr 16 and 0xff).toByte(), (intaddr shr 24 and 0xff).toByte()
            )
            result = InetAddress.getByAddress(byteaddr)
        } catch (ex: UnknownHostException) {
            Log.w("Caster", String.format("getDeviceIpAddress Error: %s", ex.message))
        }
        return result
    }

}