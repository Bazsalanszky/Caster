package eu.toldi.balazs.caster.helpers

import android.media.VolumeProvider
import android.os.Parcel
import android.os.Parcelable
import androidx.media.VolumeProviderCompat
import eu.toldi.balazs.caster.ChromeCastHelper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


class ChromecastVolumeProviderCompat(currentVolume: Float) : VolumeProviderCompat( VOLUME_CONTROL_ABSOLUTE,100,(currentVolume*100).toInt()) {
    override fun onSetVolumeTo(volume: Int) {
        GlobalScope.launch(Dispatchers.IO) {
            ChromeCastHelper.setVolume(volume.toFloat() / 100.0f)
        }
    }

    override fun onAdjustVolume(direction: Int) {
        GlobalScope.launch(Dispatchers.IO) {
            ChromeCastHelper.setVolume(ChromeCastHelper.chromeCast.status.volume.level + direction.toFloat()/1000.0f)
        }
    }
}
