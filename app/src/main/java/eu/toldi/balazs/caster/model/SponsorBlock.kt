package eu.toldi.balazs.caster.model

import com.beust.klaxon.*
import java.net.URL

private val klaxon = Klaxon()

class SponsorBlock(elements: Collection<SponsorBlockElement>) : ArrayList<SponsorBlockElement>(elements) {
    fun toJson() = klaxon.toJsonString(this)


    companion object {
        fun fromJson(json: String) = SponsorBlock(klaxon.parseArray<SponsorBlockElement>(json)!!)
        fun fromID(id: String) = SponsorBlock(klaxon.parseArray<SponsorBlockElement>(URL(url+id).readText())!!)
        private const val url = "https://sponsor.ajay.app/api/skipSegments?videoID="
    }
}

data class SponsorBlockElement (
    val category: String,
    val actionType: String,
    val segment: List<Double>,

    @Json(name = "UUID")
    val uuid: String,

    val locked: Long,
    val votes: Long,
    val videoDuration: Double,
    val userID: String,
    val description: String
)