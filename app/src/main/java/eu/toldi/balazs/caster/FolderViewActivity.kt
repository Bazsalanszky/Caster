package eu.toldi.balazs.caster

import android.annotation.SuppressLint
import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.MaterialTheme
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.Role.Companion.Image
import androidx.compose.ui.text.input.KeyboardType.Companion.Uri
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.documentfile.provider.DocumentFile
import coil.compose.AsyncImage
import coil.compose.SubcomposeAsyncImage
import coil.compose.rememberImagePainter
import coil.size.OriginalSize

import coil.transform.CircleCropTransformation
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.HorizontalPager
import com.google.accompanist.pager.rememberPagerState
import eu.toldi.balazs.caster.helpers.FileCacheHelper
import eu.toldi.balazs.caster.services.ChromecastManagerService
import eu.toldi.balazs.caster.ui.theme.CasterTheme
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import su.litvak.chromecast.api.v2.ChromeCast
import java.io.File
import java.util.*

class FolderViewActivity : ComponentActivity() {

    companion object {
        var chromecastRecv: ChromeCast? = null
        var folderRecv: DocumentFile? = null
    }

    @SuppressLint("CoroutineCreationDuringComposition")
    @OptIn(ExperimentalPagerApi::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (chromecastRecv == null || folderRecv == null) finish()
        val chromeCast: ChromeCast = chromecastRecv!!
        val folder: DocumentFile = folderRecv!!
        val cacheHelper = FileCacheHelper(applicationContext)
        setContent {
            CasterTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colorScheme.background) {
                    Column(modifier = Modifier.fillMaxSize()) {
                        MenuBar(title = folder.name!!)
                        val images = folder.listFiles()
                            .filter { it.name?.endsWith(".png") ?: false || it.name?.endsWith(".jpg") ?: false }
                        //cacheHelper.cacheThis(images.map { it.uri })
                        Log.e("CastFile", images.size.toString())
                        val row_count = images.size / 3 + 1
                        /*LazyColumn(modifier = Modifier.fillMaxWidth()) {
                            items(row_count) { index ->
                                Row(modifier = Modifier.fillMaxWidth()){
                                    for (i in 0 until 3){
                                        if(index+i >= images.size) break
                                        val imageFile = if (images[index+i].exists()) {
                                            cacheHelper.cacheThis(listOf(images[index+i].uri))
                                            File(cacheDir, cacheHelper.tryFileName(images[index+i].uri))
                                        } else {
                                            File(cacheDir, cacheHelper.tryFileName(images[index+i].uri))
                                        }
                                        Card(modifier = Modifier
                                            .padding(all = 5.dp)
                                            .weight(1f)) {
                                            Column {
                                                Column(
                                                    modifier = Modifier
                                                        .height(100.dp)
                                                        .width(100.dp)
                                                        .padding(10.dp)
                                                ) {
                                                    Image(
                                                        painter = rememberImagePainter(
                                                            data = imageFile

                                                        ),
                                                        contentDescription = null,
                                                        modifier = Modifier.fillMaxWidth()
                                                    )
                                                }
                                                //Text(imageFile.name)
                                            }
                                        }
                                    }
                                }
                            }
                        }*/
                        val pagerState = rememberPagerState()
                        if (!pagerState.isScrollInProgress) {
                            val currentPage = pagerState.currentPage
                            Log.e("Caster_IMG", currentPage.toString())
                            GlobalScope.launch(Dispatchers.IO) {
                                Thread.sleep(1000)
                                if (pagerState.currentPage == currentPage && !pagerState.isScrollInProgress) {
                                    val imageFile = if (images[currentPage].exists()) {
                                        cacheHelper.cacheThis(listOf(images[currentPage].uri))
                                        File(cacheDir, cacheHelper.tryFileName(images[currentPage].uri))
                                    } else {
                                        File(cacheDir, cacheHelper.tryFileName(images[currentPage].uri))
                                    }
                                    val link = "http:/${ChromeCastHelper.getIPv4Address()}:${ChromecastManagerService.PORT}/assets/${
                                        imageFile.name
                                    }"
                                    Log.e("Caster_IMG","Casting image with url: $link")
                                    val result = ChromeCastHelper.castLink(
                                      link
                                    ){ Log.e("Caster_IMG","Casting done")}
                                }
                            }
                        }
                        HorizontalPager(
                            modifier = Modifier.fillMaxSize(),
                            count = images.size,
                            state = pagerState

                        ) { index ->
                            val imageFile = if (images[index].exists()) {
                                cacheHelper.cacheThis(listOf(images[index].uri))
                                File(cacheDir, cacheHelper.tryFileName(images[index].uri))
                            } else {
                                File(cacheDir, cacheHelper.tryFileName(images[index].uri))
                            }

                            Card(
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .padding(all = 5.dp)
                            ) {
                                Column(modifier = Modifier.fillMaxWidth()) {
                                    Column(
                                        modifier = Modifier
                                            .fillMaxWidth()
                                            .height(240.dp)
                                        //                         .padding(10.dp)
                                    ) {
                                        Log.e(
                                            "Caster_IMG",
                                            "http:/${ChromeCastHelper.getIPv4Address()}:${ChromecastManagerService.PORT}/assets/${imageFile.name}"
                                        )
                                        SubcomposeAsyncImage(
                                            model = imageFile,
                                            contentDescription = imageFile.name,
                                            loading = {
                                                CircularProgressIndicator()
                                            },
                                            modifier = Modifier.fillMaxWidth()
                                        )

                                    }
                                    Text(imageFile.name)
                                }

                            }
                        }
                    }
                }
            }
        }
    }


    @Composable
    fun MenuBar(title: String) {
        TopAppBar(
            title = {
                Text(title)
            },
            navigationIcon = {
                IconButton(onClick = { finish() }) {
                    Icon(
                        Icons.Filled.ArrowBack,
                        contentDescription = stringResource(id = R.string.back)
                    )
                }
            }
        )
    }

    @Preview(showBackground = true)
    @Composable
    fun DefaultPreview() {
        CasterTheme {

        }
    }
}

@Composable
fun imageGrid(context : Context, folderArg: String) {
    Log.e("Base64",folderArg)
    val folder = DocumentFile.fromSingleUri(context, android.net.Uri.parse(Base64.getDecoder().decode(folderArg).toString()))!!
    val images = folder.listFiles()
        .filter { it.name?.endsWith(".png") ?: false || it.name?.endsWith(".jpg") ?: false }

    val cacheHelper = FileCacheHelper(context)
    val row_count = images.size / 3 + 1
    LazyColumn(modifier = Modifier.fillMaxWidth()) {
        items(row_count) { index ->
            Row(modifier = Modifier.fillMaxWidth()) {
                for (i in 0 until 3) {
                    if (index + i >= images.size) break
                    val imageFile = if (images[index + i].exists()) {
                        cacheHelper.cacheThis(listOf(images[index + i].uri))
                        File(context.cacheDir, cacheHelper.tryFileName(images[index + i].uri))
                    } else {
                        File(context.cacheDir, cacheHelper.tryFileName(images[index + i].uri))
                    }
                    Card(
                        modifier = Modifier
                            .padding(all = 5.dp)
                            .weight(1f)
                    ) {
                        Column {
                            Column(
                                modifier = Modifier
                                    .height(100.dp)
                                    .width(100.dp)
                                    .padding(10.dp)
                            ) {
                                Image(
                                    painter = rememberImagePainter(
                                        data = imageFile

                                    ),
                                    contentDescription = null,
                                    modifier = Modifier.fillMaxWidth()
                                )
                            }
                        }
                    }
                }
            }
        }
    }
}

