package eu.toldi.balazs.caster

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import androidx.activity.ComponentActivity
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.compose.setContent
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.material3.ColorScheme
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.core.content.ContextCompat
import androidx.documentfile.provider.DocumentFile
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import coil.compose.AsyncImage
import coil.compose.rememberImagePainter
import com.google.accompanist.systemuicontroller.rememberSystemUiController
import com.yausername.ffmpeg.FFmpeg
import com.yausername.youtubedl_android.YoutubeDL
import com.yausername.youtubedl_android.YoutubeDLException
import eu.toldi.balazs.caster.helpers.FileCacheHelper
import eu.toldi.balazs.caster.model.ChromecastManageViewmodel
import eu.toldi.balazs.caster.services.ChromecastManagerService
import eu.toldi.balazs.caster.ui.theme.CasterTheme
import eu.toldi.balazs.caster.ui.theme.getColorScheme
import su.litvak.chromecast.api.v2.ChromeCast
import su.litvak.chromecast.api.v2.Media
import su.litvak.chromecast.api.v2.MediaStatus
import java.io.File
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern
import kotlin.collections.ArrayList
import kotlin.collections.LinkedHashMap


data class BottomNavItem(
    val label: String,
    val icon: ImageVector,
    val route: String,
)

object Constants {
    val BottomNavItems = listOf(
        BottomNavItem(
            label = "Home",
            icon = Icons.Filled.Home,
            route = "home"
        ),
        BottomNavItem(
            label = "Gallery",
            icon = Icons.Filled.Image,
            route = "gallery"
        ),
        BottomNavItem(
            label = "Settings",
            icon = Icons.Filled.Settings,
            route = "settings"
        )
    )

}

class ChromecastManagerActivity : ComponentActivity() {
    companion object {
        var chromeCast_: ChromeCast? = null
        const val METADATA_THUMB = "thumb"
    }

    val pattern =
        "https?:\\/\\/(?:[0-9A-Z-]+\\.)?(?:youtu\\.be\\/|youtube\\.com\\S*[^\\w\\-\\s])([\\w\\-]{11})(?=[^\\w\\-]|$)(?![?=&+%\\w]*(?:['\"][^<>]*>|<\\/a>))[?=&+%\\w]*"

    val compiledPattern: Pattern =
        Pattern.compile(pattern, Pattern.CASE_INSENSITIVE)

    lateinit var colorScheme: ColorScheme
    lateinit var prefs: SharedPreferences

    @Composable
    fun BottomNavigationBar(navController: NavHostController) {

        BottomNavigation(
            backgroundColor = colorScheme.secondary,
            contentColor = colorScheme.onSecondary
        ) {
            // observe the backstack
            val navBackStackEntry by navController.currentBackStackEntryAsState()

            // observe current route to change the icon
            // color,label color when navigated
            val currentRoute = navBackStackEntry?.destination?.route

            // Bottom nav items we declared
            Constants.BottomNavItems.forEach { navItem ->

                // Place the bottom nav items
                BottomNavigationItem(

                    // it currentRoute is equal then its selected route
                    selected = currentRoute == navItem.route,

                    // navigate on click
                    onClick = {
                        navController.navigate(navItem.route)
                    },

                    // Icon of navItem
                    icon = {
                        Icon(imageVector = navItem.icon, contentDescription = navItem.label)
                    },

                    // label
                    label = {
                        Text(text = navItem.label)
                    },
                    alwaysShowLabel = false
                )
            }
        }
    }

    private lateinit var chromeCast: ChromeCast
    private lateinit var viewModel: ChromecastManageViewmodel
    private lateinit var navController: NavHostController

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        prefs = getSharedPreferences(
            "eu.toldi.Caster", Context.MODE_PRIVATE
        )
        if (chromeCast_ == null)
            finish()
        chromeCast = chromeCast_ as ChromeCast
        ChromeCastHelper.chromeCast = chromeCast
        try {
            YoutubeDL.getInstance().init(application)
            FFmpeg.getInstance().init(application)
        } catch (e: YoutubeDLException) {
            Log.e("Caster", "failed to initialize youtubedl-android", e)
        }
        Intent(this, ChromecastManagerService::class.java).also {
            it.action = ChromecastManagerService.ACTION_INIT
            it.putExtra("CHROMECAST_ADDRESS", chromeCast.address)
            it.putExtra("CHROMECAST_NAME", chromeCast.title)
            ContextCompat.startForegroundService(this, it)
        }
        setContent {
            CasterTheme {
                colorScheme = getColorScheme()

                val systemUiController = rememberSystemUiController()
                systemUiController.setStatusBarColor(
                    color = colorScheme.primary,
                    darkIcons = true
                )
                // remember navController so it does not
                // get recreated on recomposition
                navController = rememberNavController()

                Surface(color = colorScheme.background, contentColor = colorScheme.onBackground) {

                    // Scaffold Component
                    Scaffold(
                        backgroundColor = colorScheme.background,
                        contentColor = colorScheme.onBackground,
                        // Bottom navigation
                        bottomBar = {
                            BottomNavigationBar(navController = navController)
                        },
                        content = { padding ->
                            // Navhost: where screens are placed
                            NavHostContainer(navController = navController, padding = padding)
                        }
                    )
                }
            }

        }
    }


    @Composable
    fun NavHostContainer(
        navController: NavHostController,
        padding: PaddingValues
    ) {

        NavHost(
            navController = navController,

            // set the start destination as home
            startDestination = "home",

            // Set the padding provided by scaffold
            modifier = Modifier.padding(paddingValues = padding),

            builder = {

                // route : Home
                composable("home") {
                    Column {
                        MenuBar()
                        HomeScreen()
                    }
                }

                // route : search
                composable("gallery") {
                    Column {
                        MenuBar()
                        ImageScreen()
                    }
                }

                // route : profile
                composable("settings") {
                    Column {
                        MenuBar()
                        SettingsScreen()
                    }
                }

                composable("folderView/{folder}", listOf(navArgument("folder") { type = NavType.StringType}))
                {  backStackEntry ->
                Column {
                        MenuBar()
                        imageGrid(context = application, folderArg = backStackEntry.arguments?.getString("folder")!! )
                    }
                }
            })

    }

    @Composable
    fun mediaStatus() {
        val mediaStatus = viewModel.mediaStatus.value

        if (mediaStatus != null) {
            val nowPlaying =
                if (mediaStatus.media.metadata != null) mediaStatus.media.metadata[Media.METADATA_TITLE] else ""


            Text(text = stringResource(id = R.string.now_playing) + ": $nowPlaying")
            var sliderPosition by remember { mutableStateOf(0.0f) }
            var sliderMoving by remember { mutableStateOf(false) }
            if (mediaStatus.media.duration != null) {


                if (!sliderMoving)
                    sliderPosition =
                        (mediaStatus.currentTime / mediaStatus.media.duration).toFloat()
            }
            if (mediaStatus.media.metadata[Media.METADATA_IMAGES] != null) {
                val images =
                    mediaStatus.media.metadata[Media.METADATA_IMAGES] as ArrayList<LinkedHashMap<String, String>>
                if (images.size >= 1 && images[0].containsKey("url")) {
                    Log.e("Caster", images[0]["url"].toString())
                    Image(
                        painter = rememberImagePainter(images[0]["url"]),
                        contentDescription = null,
                        modifier = Modifier.fillMaxWidth()
                    )
                }
            } else if (mediaStatus.media.metadata[METADATA_THUMB] != null) {
                val image =
                    mediaStatus.media.metadata[METADATA_THUMB] as String
                Log.e("Caster", image)
                AsyncImage(
                    model = image,
                    contentDescription = null,
                    modifier = Modifier.fillMaxWidth()
                )
            }
            if (mediaStatus.media.contentType.startsWith("video/")) {
                Row(
                    Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {
                    Text(
                        text = String.format(
                            "%02d:%02d",
                            ((mediaStatus.currentTime % 3600) / 60).toInt(),
                            (mediaStatus.currentTime % 60).toInt()
                        )
                    )
                    Text(
                        text = String.format(
                            "%02d:%02d",
                            ((mediaStatus.media.duration % 3600) / 60).toInt(),
                            (mediaStatus.media.duration % 60).toInt()
                        )
                    )
                }
                Slider(value = sliderPosition, onValueChange = {
                    sliderPosition = it
                    sliderMoving = true
                }, onValueChangeFinished = {
                    viewModel.seek(sliderPosition.toDouble())
                    sliderMoving = false
                })
            }
        }
    }


    @Composable
    fun MenuBar() {
        val pickPictureLauncher = rememberLauncherForActivityResult(
            ActivityResultContracts.OpenDocumentTree()
        ) { fileUri ->
            if (fileUri != null) {
                DocumentFile.fromTreeUri(applicationContext, fileUri)
                    ?.let { viewModel.addFolder(it) }
            }
        }
        TopAppBar(
            title = {
                Text("Caster")
            },
            navigationIcon = {
                IconButton(onClick = { finish() }) {
                    Icon(
                        Icons.Filled.ArrowBack,
                        contentDescription = stringResource(id = R.string.back)
                    )
                }
            },
            actions = {
                Row {
                    IconButton(onClick = {
                        pickPictureLauncher.launch(Uri.EMPTY)
                    }) {
                        Icon(
                            Icons.Filled.Folder,
                            contentDescription = stringResource(id = R.string.back)
                        )
                    }
                }
            },
            backgroundColor = colorScheme.primary,
            contentColor = colorScheme.onPrimary
        )
    }

    @Composable
    fun PlayBackControl() {
        val mediaStatus = if (this::viewModel.isInitialized)
            viewModel.mediaStatus.value
        else null
        Row {

            IconButton(onClick = {
                if (mediaStatus != null) {
                    viewModel.seek((mediaStatus.currentTime - 10) / mediaStatus.media.duration)
                }
            }, enabled = mediaStatus != null) {
                Icon(
                    Icons.Filled.FastRewind,
                    contentDescription = stringResource(id = R.string.rewind)
                )
            }
            IconButton(onClick = {

                if (mediaStatus != null) {
                    if (mediaStatus.playerState == MediaStatus.PlayerState.PLAYING) {
                        viewModel.pause()
                    }
                    if (mediaStatus.playerState == MediaStatus.PlayerState.PAUSED) {
                        viewModel.play()
                    }
                }

            }, enabled = mediaStatus != null) {
                when {
                    mediaStatus == null || mediaStatus.playerState == MediaStatus.PlayerState.PAUSED -> Icon(
                        Icons.Filled.PlayArrow,
                        contentDescription = stringResource(id = R.string.resume)
                    )
                    mediaStatus.playerState == MediaStatus.PlayerState.PLAYING -> Icon(
                        Icons.Filled.Pause,
                        contentDescription = stringResource(id = R.string.pause)
                    )
                    else -> Icon(
                        Icons.Filled.PlayArrow,
                        contentDescription = stringResource(id = R.string.resume)
                    )
                }


            }
            IconButton(onClick = {
                viewModel.stopApp()
            }, enabled = mediaStatus != null) {
                Icon(
                    Icons.Filled.Stop,
                    contentDescription = stringResource(id = R.string.stop)
                )
            }

            IconButton(onClick = {
                if (mediaStatus != null) {
                    viewModel.seek((mediaStatus.currentTime + 10) / mediaStatus.media.duration)
                }
            }, enabled = mediaStatus != null) {
                Icon(
                    Icons.Filled.FastForward,
                    contentDescription = stringResource(id = R.string.fastforward)
                )
            }
        }
    }

    @Composable
    fun showNowPlaying() {
        /*var sliderPosition by remember { mutableStateOf() }
        Text(text = sliderPosition.toString())
        Slider(value = sliderPosition, onValueChange = { sliderPosition = it })*/
    }

    @Preview(showBackground = true)
    @Composable
    fun DefaultPreview2() {
        CasterTheme {
            Column {
                MenuBar()
                Column(
                    modifier = Modifier
                        .padding(6.dp)
                        .fillMaxWidth()
                        .fillMaxHeight(),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    var text by remember { mutableStateOf("") }

                    OutlinedTextField(
                        value = text,
                        onValueChange = {
                            text = it
                        },
                        label = { Text(stringResource(id = R.string.cast_url)) },
                        modifier = Modifier.padding(vertical = 4.dp)
                    )
                    Button(
                        onClick = {},
                        modifier = Modifier.padding(vertical = 4.dp)
                    ) {
                        Text(text = stringResource(id = R.string.cast))
                    }
                    //  val viewModel = ViewModelProvider(this).get(ChromecastManageViewmodel::class.java)
                    PlayBackControl()
                    Text(text = stringResource(id = R.string.now_playing) + ": Some video")
                    var sliderPosition by remember { mutableStateOf(0f) }
                    Slider(value = sliderPosition, onValueChange = { sliderPosition = it })
                }
            }
        }
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
            viewModel.increaseVolume()

            return true
        }
        if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
            viewModel.decreaseVolume()

            return true
        }
        return super.onKeyDown(keyCode, event)
    }

    @Composable
    fun HomeScreen() {
        // Column Composable,
        Column(
            modifier = Modifier
                .fillMaxSize(),
            // Parameters set to place the items in center
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ) {
            viewModel =
                ViewModelProvider(this@ChromecastManagerActivity).get(ChromecastManageViewmodel::class.java)
            viewModel.chromeCast = chromeCast
            // A surface container using the 'background' color from the theme
            //viewModel.fetchMediaStatus()

            Column(
                modifier = Modifier
                    .padding(6.dp)
                    .fillMaxWidth()
                    .fillMaxHeight(),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                var text by remember { mutableStateOf("") }

                OutlinedTextField(
                    value = text,
                    onValueChange = {
                        text = it
                    },
                    label = { Text(stringResource(id = R.string.cast_url)) },
                    modifier = Modifier.padding(vertical = 4.dp),
                    colors = TextFieldDefaults.outlinedTextFieldColors(
                        cursorColor = colorScheme.primary,
                        focusedBorderColor =
                        colorScheme.primary.copy(alpha = ContentAlpha.high),
                        focusedLabelColor =  colorScheme.primary
                    )
                )
                var castEnabled by remember { mutableStateOf(true) }
                Button(
                    enabled = castEnabled,
                    colors = ButtonDefaults.buttonColors(
                        backgroundColor = colorScheme.primary,
                        contentColor = colorScheme.onPrimary
                    ),
                    onClick = {
                        castEnabled = false
                        val link = text
                        viewModel.castLink(link) {
                            if (prefs.getBoolean("sponsorblock_enabled", false)) {
                                val matcher: Matcher = compiledPattern.matcher(link)
                                if (matcher.find()) {
                                    val id = matcher.group(1)
                                    Log.e("Caster_SB", id)
                                    val intent = Intent(
                                        applicationContext,
                                        ChromecastManagerService::class.java
                                    ).apply {
                                        action = ChromecastManagerService.ACTION_SET_YT_ID
                                        putExtra("yt_id", id)
                                        putExtra("media_url", link)
                                    }
                                    application.startService(intent);
                                }
                            }
                            castEnabled = true

                        }
                    },
                    modifier = Modifier.padding(vertical = 4.dp)
                ) {
                    Text(text = stringResource(id = R.string.cast))
                }
                if (castEnabled.not()) {
                    CircularProgressIndicator(color = colorScheme.secondary)
                }
                PlayBackControl()
                mediaStatus()
            }
        }
    }


    @Composable
    fun SettingsScreen() {
        // Column Composable,
        Column(
            modifier = Modifier
                .fillMaxSize(),
            // parameters set to place the items in center
        ) {
            Row(
                Modifier.padding(all = 10.dp),
                horizontalArrangement = Arrangement.Start,
                verticalAlignment = Alignment.CenterVertically
            ) {
                Text("Enable SponsorBlock")
                var switched by remember {
                    mutableStateOf(
                        prefs.getBoolean(
                            "sponsorblock_enabled",
                            false
                        )
                    )
                }
                Switch(checked = switched, onCheckedChange = {
                    switched = !switched
                    prefs.edit().putBoolean("sponsorblock_enabled", switched).apply()
                })
            }
        }
    }

    @Composable
    fun ImageScreen() {
        // Column Composable,
        Column(
            modifier = Modifier
                .fillMaxSize(),
        ) {

            val folders = viewModel.folderListState.value
            val cacheHelper = FileCacheHelper(applicationContext)
            LazyColumn() {
                items(folders.size) { index ->
                    Card(modifier = Modifier
                        .fillMaxWidth()
                        .clickable {
                           /* val intent = Intent(applicationContext, FolderViewActivity::class.java)
                            FolderViewActivity.chromecastRecv = chromeCast
                            FolderViewActivity.folderRecv = folders[index]
                            startActivity(intent)*/

                            navController.navigate("folderView/${Base64.getEncoder().encodeToString(folders[index].uri.toString().toByteArray())}")
                        }) {
                        Row(
                            modifier = Modifier
                                .fillMaxSize(),
                            // parameters set to place the items in center
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            val image = folders[index].listFiles()
                                .firstOrNull {
                                    it.name?.endsWith(".png") ?: false || it.name?.endsWith(
                                        ".jpg"
                                    ) ?: false
                                }
                            if (image != null) {
                                cacheHelper.cacheThis(listOf(image.uri))

                                val imageFile = File(cacheDir, cacheHelper.tryFileName(image.uri))
                                Column(
                                    modifier = Modifier
                                        .height(80.dp)
                                        .width(80.dp)
                                        .padding(10.dp)
                                ) {
                                    Log.e(null, imageFile.name.toString())
                                    Image(
                                        painter = rememberImagePainter(imageFile),
                                        contentDescription = null,
                                        modifier = Modifier.fillMaxSize()
                                    )
                                }
                            }
                            Text(text = folders[index].name!!)
                        }
                    }
                }
            }
        }
    }
}