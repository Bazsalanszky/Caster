package eu.toldi.balazs.caster

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.net.wifi.WifiManager
import android.os.Bundle
import android.os.Parcelable
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Refresh
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.yausername.youtubedl_android.YoutubeDL
import com.yausername.youtubedl_android.YoutubeDLException
import eu.toldi.balazs.caster.helpers.FileCacheHelper
import eu.toldi.balazs.caster.model.ChromeCastViewModel
import eu.toldi.balazs.caster.services.ChromecastManagerService
import eu.toldi.balazs.caster.ui.theme.CasterTheme
import kotlinx.coroutines.launch
import su.litvak.chromecast.api.v2.ChromeCast

class ShareRecieverActivity : MainActivity() {

    @SuppressLint("CoroutineCreationDuringComposition")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (intent?.action != Intent.ACTION_SEND ) {
            finish()
        }
        Log.d(null, intent.type.toString())
        val link = when {
            intent.type == "text/plain" -> intent.getStringExtra(Intent.EXTRA_TEXT) as String
            intent.type?.startsWith("video/") == true ||
                    intent.type?.startsWith("image/") == true -> {
                val uri = (intent.getParcelableExtra<Parcelable>(Intent.EXTRA_STREAM) as? Uri)
                if(uri is Uri) {
                    val fileCache = FileCacheHelper(applicationContext)
                    fileCache.cacheThis(listOf(uri))
                    "http://" + ChromeCastHelper.getIPv4Address()?.hostAddress + ":" + ChromecastManagerService.PORT + "/assets/" + fileCache.tryFileName(
                        uri
                    )
                }else ""


            }
            else -> {
                ""
            }
        }
        if (link == "") finish()
        try {
            YoutubeDL.getInstance().init(application)
        } catch (e: YoutubeDLException) {
            Log.e("Caster", "failed to initialize youtubedl-android", e)
        }
        setContent {
            CasterTheme {
                if (!isViewModelInitialised())
                    viewModel = ViewModelProvider(this).get(ChromeCastViewModel::class.java)
                viewModel.startScanning()
                val chromeCastState = viewModel.chromeCasts.observeAsState(emptyList())
                val chromeCasts = chromeCastState.value
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    Column {
                        MenuBar {
                            viewModel.refresh()
                        }
                        var enabled by remember {
                            mutableStateOf(true)
                        }
                        LazyColumn(
                            modifier = Modifier
                                .padding(all = 4.dp)
                                .fillMaxWidth(),
                            horizontalAlignment = Alignment.CenterHorizontally
                        ) {
                            item {
                                if (chromeCasts.isNotEmpty())
                                    Text(text = stringResource(id = R.string.available_chromecasts))
                                else {
                                    Column(
                                        modifier = Modifier.fillMaxSize(),
                                        horizontalAlignment = Alignment.CenterHorizontally
                                    ) {
                                        Text(stringResource(id = R.string.looking_for_devices))
                                        CircularProgressIndicator()
                                    }
                                }
                            }
                            items(chromeCasts.size) { index ->
                                showChromeCastButton(
                                    enabled = enabled,
                                    onEnableChanged = { enabled = it },
                                    chromeCast = chromeCasts[index],
                                    link = link
                                )
                            }
                            item {
                                if (enabled.not()) {
                                    CircularProgressIndicator()
                                }
                            }
                        }
                    }
                }
            }
        }
    }


    @Composable
    fun showChromeCastButton(
        enabled: Boolean,
        onEnableChanged: (Boolean) -> Unit,
        chromeCast: ChromeCast,
        link: String
    ) {
        showChromeCastButton(chromeCast = chromeCast, buttonCallBack = {
            if (enabled) {
                onEnableChanged(false)
                Intent(this, ChromecastManagerService::class.java).also {
                    it.action = ChromecastManagerService.ACTION_INIT
                    it.putExtra("CHROMECAST_ADDRESS", chromeCast.address)
                    it.putExtra("CHROMECAST_NAME", chromeCast.title)
                    ContextCompat.startForegroundService(this, it)
                }
                lifecycleScope.launch {
                    ChromeCastHelper.chromeCast = chromeCast
                    ChromeCastHelper.castLink(link) {
                        //finish()
                    }
                }
            }
        })
    }

    @Composable
    fun MenuBar(refresh: () -> Unit) {
        TopAppBar(
            title = {
                Text(stringResource(id = R.string.title_activity_share_reciever))
            },
            actions = {
                Row {
                    IconButton(onClick = refresh) {
                        Icon(
                            Icons.Filled.Refresh,
                            contentDescription = stringResource(id = R.string.refresh)
                        )
                    }
                }
            }
        )
    }


    @Preview(showBackground = true)
    @Composable
    override fun DefaultPreview() {
        CasterTheme {
            MenuBar {}
        }
    }
}