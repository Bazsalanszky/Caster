package eu.toldi.balazs.caster.services

import android.app.Notification
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import android.os.Build
import android.os.IBinder
import android.support.v4.media.session.MediaControllerCompat
import android.support.v4.media.session.MediaSessionCompat
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.media.VolumeProviderCompat
import eu.toldi.balazs.caster.App.Companion.CHANNEL_ID
import eu.toldi.balazs.caster.ChromeCastHelper
import eu.toldi.balazs.caster.ChromecastManagerActivity
import eu.toldi.balazs.caster.R
import eu.toldi.balazs.caster.helpers.ChromecastVolumeProviderCompat
import eu.toldi.balazs.caster.model.SponsorBlock
import io.ktor.application.*
import io.ktor.features.*
import io.ktor.http.*
import io.ktor.http.content.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.server.engine.*
import io.ktor.server.jetty.*
import io.ktor.websocket.*
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.IO
import su.litvak.chromecast.api.v2.ChromeCast
import su.litvak.chromecast.api.v2.Media
import su.litvak.chromecast.api.v2.MediaStatus
import java.io.File


class ChromecastManagerService : Service() {
    companion object {
        const val ACTION_INIT = "action_init"
        const val ACTION_PLAY = "action_play"
        const val ACTION_PAUSE = "action_pause"
        const val ACTION_REWIND = "action_rewind"
        const val ACTION_FAST_FORWARD = "action_fast_foward"
        const val ACTION_SET_YT_ID = "action_set_yt_id"
        const val ACTION_NEXT = "action_next"
        const val ACTION_PREVIOUS = "action_previous"
        const val ACTION_STOP = "action_stop"
        const val ACTION_SETFILE = "action_file"

        const val PORT = 3080
    }

    private val mMediaPlayer = MediaPlayer()
    private lateinit var mVolmeProvider : VolumeProviderCompat;
    private lateinit var mSession: MediaSessionCompat
    private lateinit var mController: MediaControllerCompat
    private lateinit var pendingIntent : PendingIntent
    private var mediaStatus: MediaStatus? = null
    private var file : File? = null
    private var yt_video :YoutubeVideo? = null

    override fun onBind(p0: Intent?): IBinder? = null
    private lateinit var chromeCast: ChromeCast
    private val server by lazy {
        embeddedServer(Jetty, PORT, watchPaths = emptyList()) {
            install(WebSockets)
            install(CallLogging)
            routing {
                get("/") {
                    if(file == null) {
                        call.respondText(
                            text = "Hello!! You are here in ${Build.MODEL}",
                            contentType = ContentType.Text.Plain
                        )
                    }else{

                        call.respondFile(file!!)
                    }
                }
                static("assets") {
                    staticRootFolder = applicationContext.cacheDir
                    files(".")
                }
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        when(intent!!.action) {
            ACTION_INIT -> initAction(intent)
            ACTION_PLAY -> mController.transportControls.play()
            ACTION_PAUSE -> mController.transportControls.pause()
            ACTION_FAST_FORWARD -> mController.transportControls.fastForward()
            ACTION_REWIND -> mController.transportControls.rewind()
            ACTION_SET_YT_ID -> setYTID(intent)
        }

        return START_REDELIVER_INTENT
    }

    private fun setYTID(intent: Intent) {
        CoroutineScope(IO).launch  {
            try {
                val id = intent.getStringExtra("yt_id")!!
                val url = intent.getStringExtra("media_url")!!
                yt_video = YoutubeVideo(id,url, SponsorBlock.fromID(id))
                Log.e(null,yt_video.toString())
            }catch (e: Exception) {
                Log.e("CasterService",e.stackTraceToString())
            }
        }
    }


    private fun initAction(intent: Intent?){
        initMediaSessions()
        val address =
            intent?.getStringExtra("CHROMECAST_ADDRESS") ?: throw IllegalArgumentException()
        val name = intent?.getStringExtra("CHROMECAST_NAME") ?: "Chromecast"

        Log.e(null, address)
        chromeCast = ChromeCast(address)
        chromeCast.name = name
        ChromecastManagerActivity.chromeCast_ = chromeCast
        val notificationIntent = Intent(this, ChromecastManagerService::class.java)
        pendingIntent =
            PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_IMMUTABLE)

        val notification: Notification = NotificationCompat.Builder(this, CHANNEL_ID)
            .setContentTitle("Caster for ${chromeCast.name}@${address}")
            .setSmallIcon(R.drawable.ic_new_caster_monochrome_foreground)
            .setContentIntent(pendingIntent).build()

        startForeground(1, notification)
        CoroutineScope(IO).launch {
            var started = false
            while (!started) {
                try {
                    server.start(wait = true)
                    started = true
                    Thread.sleep(1000)
                } catch (e: Exception) {
                    Log.e(null,e.stackTraceToString())
                }
            }
        }
        val prefs = getSharedPreferences(
            "eu.toldi.Caster", Context.MODE_PRIVATE)
        CoroutineScope(IO).launch {
            var errorCount = 0
            while (true) {
                try {
                    ChromeCastHelper.chromeCast = chromeCast
                    var status = chromeCast.status
                    Log.d(null, status.applications.toString())
                    if(status.runningApp.id == ChromeCastHelper.APP_BACKDROP)
                        errorCount++
                    val status_message = if (status.runningApp != null) {
                        ChromeCastHelper.getApplicationDisplayName(status.runningApp.id)
                    } else {
                        "${chromeCast.name} is ready for casting"
                    }
                    mediaStatus = ChromeCastHelper.fetchMediaStatus()
                   if(mediaStatus != null) {
                        mVolmeProvider = ChromecastVolumeProviderCompat(mediaStatus!!.volume.level)
                        mSession.setPlaybackToRemote(mVolmeProvider)
                    }
                    if(prefs.getBoolean("sponsorblock_enabled",false) && yt_video != null){
                        try {
                            if(yt_video!!.url.startsWith("https://youtu.be/")){
                                yt_video!!.url = mediaStatus!!.media.url
                            }
                            if (mediaStatus!!.media.url != yt_video!!.url) {
                                yt_video = null
                            }
                            checkForSponsorBlock()
                        } catch (e:Exception){
                            //yt_video = null
                        }
                    }else {
                        yt_video = null
                    }

                    errorCount = 0
                    buildNotification()
                } catch (e: Exception) {
                    Log.e(null,e.stackTraceToString())
                    errorCount++
                }
                if(errorCount > 120){
                    stopForeground(true)
                    break
                }
                Thread.sleep(1000)
            }
        }
    }

    private fun checkForSponsorBlock() {
        try {


            yt_video!!.sponsorBlock.forEach {
                if (mediaStatus!!.currentTime >= it.segment[0] && mediaStatus!!.currentTime <= it.segment[1])
                    chromeCast.seek(it.segment[1])
            }
        }catch (e: Exception) {
            Log.e(null,e.stackTraceToString())
        }
    }

    private fun generateAction(
        icon: Int,
        title: String,
        intentAction: String
    ): NotificationCompat.Action {
        val intent = Intent(applicationContext, ChromecastManagerService::class.java)
        intent.action = intentAction
        val pendingIntent = PendingIntent.getService(applicationContext, 1, intent, PendingIntent.FLAG_IMMUTABLE)
        return NotificationCompat.Action(icon,title,pendingIntent)
    }


    override fun onDestroy() {
        server.stop(1_000, 2_000)
        super.onDestroy()
    }

    private suspend fun buildNotification() {
        var status = chromeCast.status
        Log.d(null, status.applications.toString())

        val status_message = if (status.runningApp != null) {
            ChromeCastHelper.getApplicationDisplayName(status.runningApp.id)
        } else {
            "${chromeCast.name} is ready for xcasting"
        }
        val notification: Notification
        val mNotificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        if (mediaStatus != null) {
            notification = if(mediaStatus!!.playerState == MediaStatus.PlayerState.PLAYING) {
                NotificationCompat.Builder(baseContext, CHANNEL_ID)
                    .setContentTitle(status_message)
                    .setSmallIcon(R.drawable.ic_caster_new_monochrome_notification)
                    .setContentText(mediaStatus!!.media.metadata[Media.METADATA_TITLE].toString())
                    .addAction(
                        generateAction(
                            R.drawable.rewind,
                            "Rewind",
                            ACTION_REWIND
                        )
                    )
                    .addAction(
                        generateAction(
                            R.drawable.pause,
                            "Pause",
                            ACTION_PAUSE
                        )
                    )
                    .addAction(
                        generateAction(
                            R.drawable.forward,
                            "Fast forward",
                            ACTION_FAST_FORWARD
                        )
                    )

                 // Apply the media style template
                 .setStyle(androidx.media.app.NotificationCompat.MediaStyle()
                     .setShowActionsInCompactView(1 /* #1: pause button \*/)
                     .setMediaSession(mSession.sessionToken))

                    .setContentIntent(pendingIntent).build()
            } else {
                NotificationCompat.Builder(baseContext, CHANNEL_ID)
                    .setContentTitle(status_message)
                    .setSmallIcon(R.drawable.ic_caster_new_monochrome_notification)
                    .setContentText(mediaStatus!!.media.metadata[Media.METADATA_TITLE].toString())
                    .addAction(
                        generateAction(
                            android.R.drawable.ic_media_rew,
                            "Rewind",
                            ACTION_REWIND
                        )
                    )
                    .addAction(
                        generateAction(
                            android.R.drawable.ic_media_play,
                            "Pause",
                            ACTION_PLAY
                        )
                    )
                    .addAction(
                        generateAction(
                            android.R.drawable.ic_media_ff,
                            "Fast forward",
                            ACTION_FAST_FORWARD
                        )
                    )

                    // Apply the media style template
                    .setStyle(androidx.media.app.NotificationCompat.MediaStyle()
                        .setShowActionsInCompactView(1 /* #1: pause button \*/)
                        .setMediaSession(mSession.sessionToken))

                    .setContentIntent(pendingIntent).build()
            }

            withContext(Dispatchers.Main) {
                mNotificationManager.notify(1, notification)
            }
        } else {
            notification = NotificationCompat.Builder(baseContext, CHANNEL_ID)
                .setContentTitle(status_message)
                .setSmallIcon(R.drawable.ic_caster_new_monochrome_notification)
                .setContentIntent(pendingIntent).build()

            withContext(Dispatchers.Main) {
                mNotificationManager.notify(1, notification)
            }
        }


    }

    private fun initMediaSessions() {
        mSession = MediaSessionCompat(applicationContext, "simple player session")
        mController = MediaControllerCompat(applicationContext, mSession.sessionToken)
        mSession.setCallback(object : MediaSessionCompat.Callback() {


            override fun onPlay() {
                super.onPlay()
                Log.e("MediaPlayerService", "onPlay")
                GlobalScope.launch(IO) {
                    if(mediaStatus != null)
                        ChromeCastHelper.play()
                }
            }


            override fun onPause() {
                super.onPause()
                Log.e("MediaPlayerService", "onPause")
                GlobalScope.launch(IO) {
                    if(mediaStatus != null)
                        ChromeCastHelper.pause()
                }
            }


            override fun onFastForward() {
                super.onFastForward()
                Log.e("MediaPlayerService", "onFastForward")
                GlobalScope.launch(IO) {
                    val mediaStatus = ChromeCastHelper.fetchMediaStatus()
                    if(mediaStatus != null)
                        ChromeCastHelper.seek(mediaStatus.currentTime+10)
                }
                //Manipulate current media here
            }

            override fun onRewind() {
                super.onRewind()
                Log.e("MediaPlayerService", "onRewind")
                GlobalScope.launch(IO) {
                    val mediaStatus = ChromeCastHelper.fetchMediaStatus()
                    if(mediaStatus != null)
                        ChromeCastHelper.seek(mediaStatus.currentTime-10)
                }
            }

            override fun onStop() {
                super.onStop()
                Log.e("MediaPlayerService", "onStop")
                //Stop media player here
                stopForeground(true)
                val intent = Intent(applicationContext, ChromecastManagerService::class.java)
                stopService(intent)
            }

        }
        )
    }

    data class YoutubeVideo(
        val id: String,
        var url: String,
        val sponsorBlock: SponsorBlock
    )
}