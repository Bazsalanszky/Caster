package eu.toldi.balazs.caster.model

import android.net.Uri
import android.util.Log
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.documentfile.provider.DocumentFile
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import eu.toldi.balazs.caster.ChromeCastHelper
import eu.toldi.balazs.caster.services.ChromecastManagerService
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import su.litvak.chromecast.api.v2.ChromeCast
import su.litvak.chromecast.api.v2.MediaStatus
import java.net.InetAddress
import java.net.NetworkInterface

class ChromecastManageViewmodel : ViewModel() {

    private val _mediaState = mutableStateOf<MediaStatus?>(null)
    val mediaStatus: State<MediaStatus?>
        get() = _mediaState
    private val _folderListState = mutableStateOf<List<DocumentFile>>(listOf())
    val folderListState : State<List<DocumentFile>>
        get() = _folderListState
    lateinit var chromeCast: ChromeCast


    init {
        viewModelScope.launch(IO) {
            fetchMediaStatus()
            while (true) {
                fetchMediaStatus()
                Thread.sleep(3000)
            }
        }
    }
/*
    override fun spontaneousEventReceived(event: ChromeCastSpontaneousEvent?) {
        Log.e(null, event?.type.toString())
        if (event != null) {
            if (event.type == ChromeCastSpontaneousEvent.SpontaneousEventType.MEDIA_STATUS) {
                _mediaState.value = event.data as MediaStatus
            }
        }
    }*/

    fun fetchMediaStatus() {
        viewModelScope.launch(IO) {
            try {
                _mediaState.value = ChromeCastHelper.fetchMediaStatus()
            } catch (e: Exception) {
                Log.e(null, e.stackTraceToString())
            }
        }
    }

    fun seek(d: Double) {
        viewModelScope.launch(IO) {
            ChromeCastHelper.seek(d)
        }
    }

    fun castLink(link : String,callBack: () -> Unit = {}) {
        viewModelScope.launch(IO) {
            ChromeCastHelper.castLink(link,callBack)
        }
    }

    fun castFromCache(fileName: String,callBack: () -> Unit = {}){
        //Log.i("Caster","http://"+getIPv4Address()?.hostAddress+":"+ChromecastManagerService.PORT+"/assets/"+fileName)
        viewModelScope.launch(IO) {
           ChromeCastHelper.castLink("http://"+getIPv4Address()?.hostAddress+":"+ChromecastManagerService.PORT+"/assets/"+fileName,callBack)
        }
    }

    fun stopApp() {
        viewModelScope.launch(IO) {
            ChromeCastHelper.stopApp()
        }
    }

    fun play() {
        viewModelScope.launch(IO) {
            ChromeCastHelper.play()
        }
    }

    fun pause() {
        viewModelScope.launch(IO) {
            ChromeCastHelper.pause()
        }
    }

    fun setVolume(f: Float) {
        viewModelScope.launch(IO) {
            ChromeCastHelper.setVolume(f)
        }
    }

    fun increaseVolume(){
        viewModelScope.launch(IO) {
            setVolume(chromeCast.status.volume.level + 0.05f)
        }
    }

    fun decreaseVolume(){
        viewModelScope.launch(IO) {
            setVolume(chromeCast.status.volume.level - 0.05f)
        }
    }

    fun addFolder(folder: DocumentFile){
        if(folder.isDirectory){
            _folderListState.value += folder
        }
    }

    fun getIPv4Address(): InetAddress? {
        NetworkInterface.getNetworkInterfaces().toList().forEach { interf ->
            interf.inetAddresses.toList().forEach { inetAddress ->
                if (!inetAddress.isLoopbackAddress && inetAddress.hostAddress.indexOf(':') < 0) {
                    return inetAddress
                }
            }
        }
        return null
    }
}