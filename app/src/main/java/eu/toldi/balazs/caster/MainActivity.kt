package eu.toldi.balazs.caster

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.graphics.ColorSpace.adapt
import android.net.wifi.WifiManager
import android.net.wifi.WifiManager.MulticastLock
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.*
import androidx.compose.material.Button
import androidx.compose.material.ButtonColors
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Card
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.material.TextFieldDefaults
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Refresh
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color.Companion.Transparent
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.ViewModelProvider
import com.google.accompanist.swiperefresh.SwipeRefresh
import com.google.accompanist.swiperefresh.rememberSwipeRefreshState
import com.google.accompanist.systemuicontroller.rememberSystemUiController
import eu.toldi.balazs.caster.model.ChromeCastViewModel
import eu.toldi.balazs.caster.ui.theme.CasterTheme
import eu.toldi.balazs.caster.ui.theme.getColorScheme
import su.litvak.chromecast.api.v2.ChromeCast


open class MainActivity : ComponentActivity() {

    protected lateinit var viewModel: ChromeCastViewModel
    protected var multicastLock: MulticastLock? = null
    protected lateinit var colorScheme: ColorScheme

    protected fun isViewModelInitialised() = ::viewModel.isInitialized
    override fun onStart() {
        super.onStart()
        val wifi = applicationContext.getSystemService(WIFI_SERVICE) as WifiManager
        multicastLock = wifi.createMulticastLock(javaClass.name)
        multicastLock!!.setReferenceCounted(true)
        multicastLock!!.acquire()
        if (!this::viewModel.isInitialized)
            viewModel = ViewModelProvider(this).get(ChromeCastViewModel::class.java)
        viewModel.startScanning()
    }

    override fun onStop() {
        super.onStop()
        if (multicastLock != null) {
            Log.i("Caster", "Releasing Mutlicast Lock...")
            multicastLock!!.release()
            multicastLock = null
        }
        viewModel.startScanning()
    }


    @SuppressLint("CoroutineCreationDuringComposition")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {

            CasterTheme {
                colorScheme = getColorScheme()
                viewModel = ViewModelProvider(this).get(ChromeCastViewModel::class.java)

                val systemUiController = rememberSystemUiController()
                systemUiController.setStatusBarColor(
                    color = colorScheme.primary,
                    darkIcons = true
                )

                val chromeCastState = viewModel.chromeCasts.observeAsState(initial = emptyList())
                val chromeCasts = chromeCastState.value
                Log.e(null, chromeCasts.toString())
                // A surface container using the 'background' color from the theme
                Surface(color = colorScheme.background, contentColor = colorScheme.onBackground) {
                    Column(
                        modifier = Modifier
                            .fillMaxWidth()
                            .fillMaxHeight()
                    ) {
                        var isAddChromecastDialogOpen by remember {
                            mutableStateOf(false)
                        }
                        MenuBar(
                            refresh = {
                                viewModel.refresh()
                            },
                            add = {
                                isAddChromecastDialogOpen = true
                            })


                        if (isAddChromecastDialogOpen) {
                            showAddChromecastDialog(dismiss = {
                                isAddChromecastDialogOpen = false
                            }) {
                                if (it.matches(Regex("^(?:[0-9]{1,3}\\.){3}[0-9]{1,3}\$")))
                                    viewModel.addChromecast(ChromeCast(it).also { chromeCast ->
                                        chromeCast.name = "Chromecast@$it"
                                    })
                            }
                        }
                        var isRefreshing by remember {
                            mutableStateOf(false)
                        }

                        SwipeRefresh(
                            state = rememberSwipeRefreshState(isRefreshing),
                            onRefresh = {
                                isRefreshing = true
                                viewModel.refresh()
                                isRefreshing = false
                            },
                            modifier = Modifier
                                .fillMaxWidth()
                                .fillMaxHeight()
                        ) {
                            LazyColumn(
                                modifier = Modifier
                                    .padding(all = 4.dp)
                                    .fillMaxWidth()
                                    .fillMaxHeight(),
                                horizontalAlignment = Alignment.CenterHorizontally
                            ) {
                                item {
                                    if (chromeCasts.isNotEmpty())
                                        Text(
                                            text = stringResource(id = R.string.available_chromecasts),
                                            color = colorScheme.onBackground
                                        )
                                    else {
                                        Column(
                                            modifier = Modifier.fillMaxSize(),
                                            horizontalAlignment = Alignment.CenterHorizontally
                                        ) {
                                            Text(
                                                text = stringResource(id = R.string.looking_for_devices),
                                                color = colorScheme.onBackground
                                            )
                                            CircularProgressIndicator(color = colorScheme.secondary)
                                        }
                                    }
                                }
                                items(chromeCasts.size) { index ->
                                    showChromeCastButton(chromeCast = chromeCasts[index])
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @Composable
    fun showAddChromecastDialog(dismiss: () -> Unit, add: (String) -> Unit) {
        var ipaddress by remember {
            mutableStateOf("")
        }
        AlertDialog(onDismissRequest = dismiss,
            title = {
                Text(
                    text = stringResource(id = R.string.add_chromecast),
                    color = colorScheme.onBackground
                )
            },
            text = {
                OutlinedTextField(
                    value = ipaddress,
                    onValueChange = {
                        ipaddress = it
                    },
                    label = {
                        Text(
                            stringResource(id = R.string.ip_address),
                            color = colorScheme.onBackground
                        )
                    },
                    modifier = Modifier.padding(vertical = 4.dp),
                    colors = TextFieldDefaults.outlinedTextFieldColors(
                        cursorColor = colorScheme.primary,
                        focusedBorderColor =
                        colorScheme.primary.copy(alpha = ContentAlpha.high),
                        focusedLabelColor = colorScheme.primary
                    )
                )
            }, buttons = {
                Row(
                    modifier = Modifier.padding(all = 8.dp),
                    horizontalArrangement = Arrangement.Center
                ) {
                    Button(
                        onClick = {
                            add(ipaddress)
                            dismiss()
                        },
                        colors = ButtonDefaults.buttonColors(
                            backgroundColor = colorScheme.primary,
                            contentColor = colorScheme.onPrimary
                        ),
                        modifier = Modifier.padding(all = 8.dp),

                        ) {
                        Text(text = stringResource(id = R.string.add_chromecast))
                    }
                    Button(
                        onClick = dismiss,
                        colors = ButtonDefaults.buttonColors(
                            backgroundColor = colorScheme.primary,
                            contentColor = colorScheme.onPrimary
                        ),
                        modifier = Modifier.padding(all = 8.dp)
                    ) {
                        Text(stringResource(id = R.string.cancel))
                    }
                }
            })
    }


    @Composable
    fun showChromeCastButton(
        chromeCast: ChromeCast, buttonCallBack: () -> Unit = {
            ChromecastManagerActivity.chromeCast_ = chromeCast
            val intent = Intent(applicationContext, ChromecastManagerActivity::class.java)
            startActivity(intent)
        }
    ) {
        Column(
            Modifier
                .fillMaxWidth()
                .padding(all = 5.dp)
        ) {
            Card(modifier = Modifier
                .fillMaxWidth()
                .clickable {
                    buttonCallBack()
                }) {
                Row {
                    val image_id = when (chromeCast.model) {
                        "Chromecast Ultra" -> R.drawable.chromecastultra
                        else -> R.drawable.chromecastv1
                    }
                    Column(
                        modifier = Modifier
                            .height(80.dp)
                            .width(80.dp)
                            .padding(10.dp)
                    ) {
                        Image(
                            painter = painterResource(id = image_id),
                            contentDescription = chromeCast.model,
                            Modifier
                                .fillMaxHeight()
                                .fillMaxWidth()

                        )
                    }
                    Column(
                        modifier = Modifier
                            .fillMaxWidth()
                            .height(80.dp),
                        verticalArrangement = Arrangement.Center
                    ) {
                        Text(
                            text = "Name: " + when {
                                chromeCast.title != null -> chromeCast.title
                                chromeCast.name != null -> chromeCast.name
                                else -> chromeCast.address
                            }
                        )
                        Text(
                            text = "Model: " + when {
                                chromeCast.model != null -> chromeCast.model
                                else -> "Unknown"
                            }
                        )
                    }
                }

            }
        }
    }

    @Composable
    fun MenuBar(refresh: () -> Unit, add: () -> Unit) {
        TopAppBar(
            title = {
                Text("Caster")
            },
            actions = {
                Row {
                    IconButton(onClick = refresh) {
                        Icon(
                            Icons.Filled.Refresh,
                            contentDescription = stringResource(id = R.string.refresh)
                        )
                    }

                    IconButton(onClick = add) {
                        Icon(
                            Icons.Filled.Add,
                            contentDescription = stringResource(id = R.string.add_chromecast)
                        )
                    }
                }
            },
            backgroundColor = colorScheme.primary,
            contentColor = colorScheme.onPrimary
        )
    }


    @Preview(showBackground = true)
    @Composable
    open fun DefaultPreview() {
        CasterTheme {
            Column {
                MenuBar({}, {})
                Column(
                    modifier = Modifier
                        .padding(all = 4.dp)
                        .fillMaxWidth(),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    showChromeCastButton(
                        chromeCast = ChromeCast("127.0.0.1").apply {
                            name = "Chromecast@127.0.0.1"
                        }
                    )
                }

            }


        }
    }
}