package eu.toldi.balazs.caster

import android.util.Log
import com.yausername.youtubedl_android.YoutubeDL
import com.yausername.youtubedl_android.YoutubeDLException
import com.yausername.youtubedl_android.YoutubeDLRequest
import com.yausername.youtubedl_android.mapper.VideoInfo
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext
import su.litvak.chromecast.api.v2.Application
import su.litvak.chromecast.api.v2.ChromeCast
import su.litvak.chromecast.api.v2.MediaStatus
import java.net.InetAddress
import java.net.NetworkInterface

object ChromeCastHelper {

    const val APP_BACKDROP = "E8C28D3C"
    const val APP_YOUTUBE = "233637DE"
    const val APP_MEDIA_RECEIVER = "CC1AD845"
    const val APP_PLEX = "06ee44ee-e7e3-4249-83b6-f5d0b6f07f34_1"
    const val APP_DASHCAST = "84912283"
    const val APP_HOME_ASSISTANT = "B12CE3CA"
    const val APP_SUPLA = "A41B766D"
    const val APP_YLEAREENA = "A9BCCB7C"
    const val APP_BUBBLEUPNP = "3927FA74"
    const val APP_BBCSOUNDS = "03977A48"
    const val APP_BBCIPLAYER = "5E81F6DB"
    const val APP_YOUTUBE_MUSIC = "2DB7CC49"

    lateinit var chromeCast: ChromeCast

    fun getApplicationDisplayName(id: String): String {
        return when (id) {
            APP_MEDIA_RECEIVER -> "Media Player"
            APP_YOUTUBE -> "Youtube"
            APP_YOUTUBE_MUSIC -> "Youtube Music"
            APP_PLEX -> "Plex"
            APP_BACKDROP -> "Idle"
            APP_DASHCAST -> "Dashcast"
            APP_HOME_ASSISTANT -> "Home Assistant"
            APP_SUPLA -> "Supla"
            APP_YLEAREENA -> "Yleareena"
            APP_BUBBLEUPNP -> "BubbleUPNP"
            APP_BBCSOUNDS -> "BBC Sounds"
            APP_BBCIPLAYER -> "BBC IPlayer"
            else -> "Unknown App $id"
        }
    }

    suspend fun fetchMediaStatus(): MediaStatus? {
        try {
            var mediaStatus : MediaStatus? = null
            withContext(IO) {
                val status = chromeCast.status
                if (status.runningApp != null) {
                    mediaStatus = chromeCast.mediaStatus
                }
            }
            return mediaStatus
        } catch (e: Exception) {
            Log.e(null, e.stackTraceToString())
        }
        return null
    }

    suspend fun seek(d: Double) {
        val mediaStatus = fetchMediaStatus()
        if (mediaStatus != null) {
            try {
                withContext(IO) {
                    try {
                        chromeCast.seek(d * mediaStatus.media.duration)
                    }catch (e:Exception) {
                        Log.e(null,e.stackTraceToString())
                    }
                }
            } catch (e: Exception) {
                Log.e(null, e.stackTraceToString())
            }
        }
    }

    suspend fun stopApp() {
        withContext(IO) {
            if (chromeCast.runningApp != null) {
                chromeCast.stopApp()
            }
        }
    }

    suspend fun play() {
        withContext(IO) {
            chromeCast.play()
        }
    }

    suspend fun pause(){
        withContext(IO) {
            chromeCast.pause()
        }
    }

    suspend fun setVolume(f: Float) {
        withContext(IO) {
            if(f >= 0.0f && f <= 1.0f) {
                try {
                    chromeCast.setVolume(f)
                }catch (e:Exception) {
                    Log.e(null,e.stackTraceToString())
                }
            }
        }
    }

    suspend fun castLink(link : String, callBack : () -> Unit = {}) : Boolean{
        var exitStatus = true
        if(link.startsWith("https://").not() && link.startsWith("http://").not()) {
            callBack.invoke()
            return false
        }
        withContext(IO){
            val request =
                YoutubeDLRequest(link)
            request.addOption("-f", "best")

            val _streamInfo = try {
                YoutubeDL.getInstance().getInfo(request)
            } catch (e: YoutubeDLException) {
                null
            }
            if (_streamInfo == null) {
                exitStatus = false
                Log.e("Caster_ChromecastHelper","Failed to fetch stream!")
                callBack.invoke()
            } else {
                try {
                    val streamInfo = _streamInfo as VideoInfo
                    val status = chromeCast.status
                    if (chromeCast.isAppAvailable(ChromeCastHelper.APP_MEDIA_RECEIVER) && !status.isAppRunning(
                            ChromeCastHelper.APP_MEDIA_RECEIVER
                        )
                    ) {
                        val app: Application =
                            chromeCast.launchApp(ChromeCastHelper.APP_MEDIA_RECEIVER)
                    }
                    while (!chromeCast.status.isAppRunning(ChromeCastHelper.APP_MEDIA_RECEIVER)) {
                        delay(100)
                    }

                    chromeCast.load(
                        streamInfo.title,
                        streamInfo.thumbnail,
                        streamInfo.url,
                        null
                    )
                } catch (e: Exception) {
                    Log.e(null, e.stackTraceToString())
                } finally {
                    callBack.invoke()
                }
            }
        }
        return exitStatus
    }
    fun getIPv4Address(): InetAddress? {
        NetworkInterface.getNetworkInterfaces().toList().forEach { interf ->
            interf.inetAddresses.toList().forEach { inetAddress ->
                if (!inetAddress.isLoopbackAddress && inetAddress.hostAddress.indexOf(':') < 0) {
                    return inetAddress
                }
            }
        }
        return null
    }
}