package eu.toldi.balazs.caster.ui.theme

import android.os.Build
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.material3.*

import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalContext

@Composable
fun getColorScheme(): ColorScheme {
    val LightColorScheme = lightColorScheme(
        primary = MaterialTheme.colors.primary,
        secondary = MaterialTheme.colors.secondary,
        tertiary = MaterialTheme.colors.secondaryVariant,
        // error, primaryContainer, onSecondary, etc.
    )
    val DarkColorScheme = darkColorScheme(
        primary = MaterialTheme.colors.primary,
        secondary = MaterialTheme.colors.secondary,
        tertiary = MaterialTheme.colors.secondaryVariant,
        // error, primaryContainer, onSecondary, etc.
    )

    val darkTheme = isSystemInDarkTheme()

    val dynamicColor = Build.VERSION.SDK_INT >= Build.VERSION_CODES.S
    return when {
        dynamicColor && darkTheme -> dynamicDarkColorScheme(LocalContext.current)
        dynamicColor && !darkTheme -> dynamicLightColorScheme(LocalContext.current)
        darkTheme -> DarkColorScheme
        else -> LightColorScheme
    }
}